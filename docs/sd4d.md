#### Selection Detection for Dummies

<img src="assets/sd4d.png" alt="Selection Detection for Dummies"  style="background:none; border:none; box-shadow:none;" />

<center>Francisco Pina Martins</center>
<center>April 2020</center>

---

### DNA

<img src="assets/DNA.jpg" style="background:none; border:none; box-shadow:none;">

<p class="fragment">**D**eoxyribo**n**ucleic **a**cid</p>

---

<section data-background="assets/Chemical-structure-of-DNA.png" data-background-size="1024px">

---

### Nucleotides

![IUPAC](assets/IUPAC.png)

IUPAC codes

---

### Compression!

<img src="assets/chromossome_to_sequence.gif" style="background:none; border:none; box-shadow:none;">

---

### Translation Genetic code

<img src="assets/codon_table.jpg" style="background:none; border:none; box-shadow:none;">

---

### Central dogma of molecular biology

<img src="assets/Central_dogma.png" style="background:none; border:none; box-shadow:none;">

|||

### Central dogma

*This states that once 'information' has passed into protein it cannot get out again. In more detail, the transfer of information from nucleic acid to nucleic acid, or from nucleic acid to protein may be possible, but transfer from protein to protein, or from protein to nucleic acid is impossible. Information means here the precise determination of sequence, either of bases in the nucleic acid or of amino acid residues in the protein.*

<p align="right">- Francis Crick, 1958</p>

---

### Heritability

<img src="assets/heritability.png" style="background:none; border:none; box-shadow:none;">

---

### Loci & alleles

<img src="assets/diploid.png" style="background:none; border:none; box-shadow:none;">

|||

### Loci & alleles

<img src="assets/tetraploid.png" style="background:none; border:none; box-shadow:none;">

---

### Phenotypes & alleles

<img src="assets/alleles_phenotypes.png" style="background:none; border:none; box-shadow:none;">

---

### Multi-genic traits

<img src="assets/Human-skin-color-chart.jpg" style="background:none; border:none; box-shadow:none;">

---

### Understandig our data

<img src="assets/RAD.png" style="background:none; border:none; box-shadow:none;">

|||

### Understandig our data

<img src="assets/ipyrad.png" style="background:none; border:none; box-shadow:none;">

|||

### Understandig our data

<img src="assets/grouping.png" style="background:none; border:none; box-shadow:none;">

---

### PHEW!

<img src="assets/Deadpool-thumbs-up_0.jpg" style="background:none; border:none; box-shadow:none;">

---

### What is [Natural] Selection?

<ul>
<li class="fragment">A mechanism of evolution</li>
  <ul>
  <li class="fragment">There is variation in **phenotypes**</li>
  <li class="fragment">Phenotypes have **differential reproduction**</li>
  <li class="fragment">There is heritability</li>
  <li class="fragment">More **advantageous** phenotypes increase in frequency</li>
  </ul>
</ul>

---

### What is [Natural] Selection?

<img src="assets/Selection.png" alt="Selection plot"  style="background:none; border:none; box-shadow:none;" />

---

### How can selection be detected?
<style>
.container{
display: flex;
}
.col{
flex: 1;
}
</style>
<div class="container">
<div class="col">
<div class="fragment">
Outlier detection
<img src="assets/KLDs.png" alt="KLD Manhattan plot" style="background:none; border:none; box-shadow:none;" />
</br>
Finds alleles with "unexpected" frequencies
<ul>
<li class="fragment">Bayescan</li>
<li class="fragment">SelEstim</li>
</ul>
</div>
</div>

<div class="col">
<div class="fragment">
Association with variable

<img src="assets/Association.png" alt="Correlation plot" style="background:none; border:none; box-shadow:none;" />
</br>
Finds alleles that vary with external variables
<ul>
<li class="fragment">Baypass</li>
<li class="fragment">LFMM</li>
</ul>
</div>

</div>

---

### Current methods limitations

<ul>
<li class="fragment" data-fragment-index="1">[Number of false positives](https://onlinelibrary.wiley.com/doi/full/10.1111/mec.14584)</li>
<div class="fragment" data-fragment-index="1" style="float: right"><img src="assets/type-i-error.jpg" alt="Spheres" style="background:none; border:none; box-shadow:none;"/></div>
<li class="fragment" data-fragment-index="2">[Single locus](https://onlinelibrary.wiley.com/doi/full/10.1111/mec.14584)</li>
<div class="fragment" data-fragment-index="2" style="float: right"><img src="assets/Hexlet_800.gif" alt="Spheres" style="background:none; border:none; box-shadow:none;"/></div>
<li class="fragment" data-fragment-index="3">[CORRELATION DOES NOT MEAN CAUSATION!](https://www.tylervigen.com/spurious-correlations)</li>
<div class="fragment" data-fragment-index="3" style="float: left"><img src="assets/spurious.png" alt="Spurious correlation plot" style="background:none; border:none; box-shadow:none;"/></div>
</ul>

---

### Methods in wide usage

<ul>
<li class="fragment">Thousands of papers rely on these methods</li>
  <ul>
  <li class="fragment">[1721 citations for Bayescan alone](https://scholar.google.pt/scholar?cites=4319645506146909873&as_sdt=2005&sciodt=0,5&hl=pt-PT)</li>
  </ul>
<li class="fragment">This led to some interesting meta-analyses</li>
  <ul>
  <li class="fragment">[Ahrens et al. 2018](https://onlinelibrary.wiley.com/doi/full/10.1111/mec.14549)</li>
  </ul>
</ul>

<img src="assets/ahrens-01.png" class="fragment" style="background:none; border:none; box-shadow:none;" />

---

### The importance of detection selection

<ul>
<li class="fragment">Understand Evolutionary History</li>
<li class="fragment">Understand phenotypic diversity</li>
<li class="fragment">Understand the response of adaptive traits</li>
  <ul>
  <li class="fragment">Ultimately required to understand how species can adapt to new conditions</li>
  </ul>
</ul>

<img src="assets/owls.jpg" class="fragment" style="background:none; border:none; box-shadow:none;" />

